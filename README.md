https://gitlab.com/rastorguevia/jse-10
# Task Manager
## Software
+ JRE
+ Java 8
+ Maven 3.6.3
## Developer
Ivan Rastorguev

email: rastorguev.i.a@yandex.ru
## Build App
```
mvn clean install
```
## Run App
```
java -jar ../target/release/bin/taskmanager.jar
```
