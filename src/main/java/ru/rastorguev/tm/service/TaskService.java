package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.api.service.ITaskService;
import ru.rastorguev.tm.entity.Task;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    @Override
    public IRepository<Task> getRepository() {
        return taskRepository;
    }

    @Override
    public void removeTaskListByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeTaskListByProjectId(projectId);
    }

    @Nullable
    @Override
    public String getTaskIdByNumber(final int number, @Nullable final List<Task> filteredListOfTasks) {
        if (filteredListOfTasks == null) return null;
        for (@NotNull final Task task: filteredListOfTasks) {
            if (task.getId().equals(filteredListOfTasks.get(number - 1).getId())) {
                return task.getId();
            }
        }
        return null;
    }

    @NotNull
    @Override
    public List<Task> taskListByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();;
        return taskRepository.taskListByUserId(userId);
    }

    @NotNull
    @Override
    public List<Task> filteredTaskListByUserIdAndInput(@NotNull final String userId, @NotNull final String input) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (input == null || input.isEmpty()) return Collections.emptyList();
        return taskRepository.filteredTaskListByUserIdAndInput(userId, input);
    }


    @NotNull
    @Override
    public List<Task> taskListByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.taskListByProjectId(projectId);
    }
}