package ru.rastorguev.tm.error;

public class FileDoesNotExistException extends Exception{

    public FileDoesNotExistException(final String message) {
        super(message);
    }
}
