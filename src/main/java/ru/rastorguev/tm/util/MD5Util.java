package ru.rastorguev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class MD5Util {

    @Nullable
    public static String mdHashCode(@Nullable final String string) {
        if (string == null || string.isEmpty()) return null;
        byte[] digest = new byte[0];

        try {
            @NotNull final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(string.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        @NotNull final BigInteger bigInt = new BigInteger(1, digest);
        @NotNull String hashResult = bigInt.toString(16);

        while(hashResult.length() < 32){
            hashResult = "0" + hashResult;
        }
        return hashResult;
    }
}