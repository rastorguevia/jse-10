package ru.rastorguev.tm.command.data.load;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.dto.DataTransferObject;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.FileDoesNotExistException;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;

@NoArgsConstructor
public class DataLoadFasterXmlJsonCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_load_fasterxml_json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load repositories FasterXML JSON";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Data load FasterXML JSON");

        @NotNull final File file = new File("./data-fasterxml.json");
        if (!Files.exists(file.toPath())) throw new FileDoesNotExistException("File does not exist");

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        @NotNull final DataTransferObject dto = mapper.readValue(file, DataTransferObject.class);
        dto.loadFromDto(serviceLocator);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}
