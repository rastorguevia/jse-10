package ru.rastorguev.tm.command.data.save;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.dto.DataTransferObject;
import ru.rastorguev.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;

@NoArgsConstructor
public class DataSaveFasterXmlXmlCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_save_fasterxml_xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save repositories FasterXML XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Data save FasterXML XML");

        @NotNull final File file = new File("./data-fasterxml.xml");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));

        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.loadDto(serviceLocator);

        mapper.writerWithDefaultPrettyPrinter().writeValue(file, dto);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}
