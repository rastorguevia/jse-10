package ru.rastorguev.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.dto.DataTransferObject;
import ru.rastorguev.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

@NoArgsConstructor
public class DataSaveJaxbXmlCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_save_jaxb_xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save repositories JAX-B XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Data save JAX-B XML");

        @NotNull final File file = new File("./data-jaxb.xml");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(DataTransferObject.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.loadDto(serviceLocator);
        marshaller.marshal(dto, new FileOutputStream(file));

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}
