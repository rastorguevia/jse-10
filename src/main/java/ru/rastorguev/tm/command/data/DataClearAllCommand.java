package ru.rastorguev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.enumerated.Role;

public class DataClearAllCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "clear_all";
    }

    @Override
    public @NotNull String getDescription() {
        return "Clear all repositories.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Clear All");

        serviceLocator.getUserService().removeAll();
        serviceLocator.getProjectService().removeAll();
        serviceLocator.getTaskService().removeAll();

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}
