package ru.rastorguev.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.dto.DataTransferObject;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.FileDoesNotExistException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;

@NoArgsConstructor
public class DataLoadJaxbXmlCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_load_jaxb_xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load repositories JAX-B XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Data load JAX-B XML");

        @NotNull final File file = new File("./data-jaxb.xml");
        if (!Files.exists(file.toPath())) throw new FileDoesNotExistException("File does not exist");

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(DataTransferObject.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        @NotNull final DataTransferObject dto = (DataTransferObject) unmarshaller.unmarshal(new FileInputStream(file));
        dto.loadFromDto(serviceLocator);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}
