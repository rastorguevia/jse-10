package ru.rastorguev.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.*;
import org.eclipse.persistence.oxm.MediaType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.dto.DataTransferObject;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.FileDoesNotExistException;

import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;

@NoArgsConstructor
public class DataLoadJaxbJsonCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_load_jaxb_json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load repositories JAX-B JSON";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Data load JAX-B JSON");

        @NotNull final File file = new File("./data-jaxb.json");
        if (!Files.exists(file.toPath())) throw new FileDoesNotExistException("File does not exist");

        @NotNull final JAXBContext jaxbContext = (JAXBContext) JAXBContextFactory.createContext(new Class[]{DataTransferObject.class}, null);
        @NotNull final JAXBUnmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, false);
        unmarshaller.setProperty(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);

        @NotNull final DataTransferObject dto = (DataTransferObject) unmarshaller.unmarshal(new StreamSource(file), DataTransferObject.class).getValue();
        dto.loadFromDto(serviceLocator);


        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}
