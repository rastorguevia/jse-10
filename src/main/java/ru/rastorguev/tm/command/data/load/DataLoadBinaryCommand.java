package ru.rastorguev.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.dto.DataTransferObject;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.FileDoesNotExistException;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public class DataLoadBinaryCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_bin_load";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load repositories - binary.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Data load");

        @NotNull final File file = new File("./data.bin");
        if (!Files.exists(file.toPath())) throw new FileDoesNotExistException("File does not exist");

        @NotNull final ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
        @NotNull final DataTransferObject dto = (DataTransferObject) in.readObject();
        dto.loadFromDto(serviceLocator);
        in.close();
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}
