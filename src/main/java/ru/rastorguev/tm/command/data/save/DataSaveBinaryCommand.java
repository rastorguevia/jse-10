package ru.rastorguev.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.dto.DataTransferObject;
import ru.rastorguev.tm.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public class DataSaveBinaryCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_bin_save";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save repositories - binary.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Data binary save");

        @NotNull final File file = new File("./data.bin");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.loadDto(serviceLocator);
        out.writeObject(dto);
        out.flush();
        out.close();
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}
