package ru.rastorguev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.enumerated.Status;

import java.io.IOException;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class ProjectEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "project_edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Project edit");
        System.out.println("Enter project ID");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final int number = Integer.parseInt(serviceLocator.getTerminalService().nextLine());
        @Nullable final String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(number, user.getId());
        @Nullable final Project project = serviceLocator.getProjectService().findOne(projectId);
        @NotNull final Project editedProject = new Project();
        editedProject.setId(project.getId());
        editedProject.setUserId(project.getUserId());
        System.out.println("Edit name? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter project name");
            editedProject.setName(serviceLocator.getTerminalService().nextLine());
        } else editedProject.setName(project.getName());
        System.out.println("Edit description? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter new description");
            editedProject.setDescription(serviceLocator.getTerminalService().nextLine());
        } else editedProject.setDescription(project.getDescription());
        System.out.println("Edit start date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter start date");
            editedProject.setStartDate(stringToDate(serviceLocator.getTerminalService().nextLine()));
        } else editedProject.setStartDate(project.getStartDate());
        System.out.println("Edit end date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter end date");
            editedProject.setEndDate(stringToDate(serviceLocator.getTerminalService().nextLine()));
        } else editedProject.setEndDate(project.getEndDate());
        System.out.println("Edit status? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter status: Planned, InProgress, Done.");
            @NotNull final String input = serviceLocator.getTerminalService().nextLine();
            switch (input) {
                case "Planned":
                    editedProject.setStatus(Status.PLANNED);
                    break;
                case "InProgress":
                    editedProject.setStatus(Status.IN_PROGRESS);
                    break;
                case "Done":
                    editedProject.setStatus(Status.DONE);
                    break;
                default:
                    System.out.println("Wrong status.");
            }
        }
        serviceLocator.getProjectService().merge(editedProject);
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}