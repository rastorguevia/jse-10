package ru.rastorguev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.util.DateUtil.*;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "project_create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Project create");
        System.out.println("Enter name");
        @NotNull final Project project = new Project();
        project.setUserId(serviceLocator.getUserService().getCurrentUser().getId());
        project.setName(serviceLocator.getTerminalService().nextLine());
        System.out.println("Enter description");
        project.setDescription(serviceLocator.getTerminalService().nextLine());
        System.out.println("Add date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Add start date DD.MM.YYYY? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
                System.out.println("Enter start date");
                project.setStartDate(stringToDate(serviceLocator.getTerminalService().nextLine()));
            }
            System.out.println("Add end date DD.MM.YYYY? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
                System.out.println("Enter end date");
                project.setEndDate(stringToDate(serviceLocator.getTerminalService().nextLine()));
            }
        }
        serviceLocator.getProjectService().persist(project);
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role [] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}