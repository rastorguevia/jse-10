package ru.rastorguev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import static ru.rastorguev.tm.view.View.printAllProjectsForUser;
import static ru.rastorguev.tm.view.View.printAllProjectsForUserByCreationDate;

public class ProjectListByCreationDateCommand extends AbstractCommand {
    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "project_list_creation";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show all projects by creation date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Project list by creation date");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUserByCreationDate(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
