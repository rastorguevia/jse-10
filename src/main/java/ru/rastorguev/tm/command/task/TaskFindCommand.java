package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.util.List;

import static ru.rastorguev.tm.view.View.printProjectsByWord;
import static ru.rastorguev.tm.view.View.printTasksByWord;

@NoArgsConstructor
public final class TaskFindCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_find";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Find task by part of title or description.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Find task");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        @NotNull final String userId = user.getId();
        System.out.println("Enter part of title or description");
        @NotNull final String input = serviceLocator.getTerminalService().nextLine();
        @NotNull final List<Task> filteredListOfTasks = serviceLocator.getTaskService().filteredTaskListByUserIdAndInput(userId, input);
        printTasksByWord(filteredListOfTasks);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}