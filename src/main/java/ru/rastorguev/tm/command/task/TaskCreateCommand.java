package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Task create");
        System.out.println("Enter Project ID");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final int number = Integer.parseInt(serviceLocator.getTerminalService().nextLine());
        @Nullable String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(number, user.getId());
        @Nullable Project project = serviceLocator.getProjectService().findOne(projectId);
        if (project == null) return;
        @NotNull final Task task = new Task(projectId);
        task.setProjectName(project.getName());
        task.setUserId(user.getId());
        System.out.println("Enter task name");
        task.setName(serviceLocator.getTerminalService().nextLine());
        System.out.println("Enter task description");
        task.setDescription(serviceLocator.getTerminalService().nextLine());
        System.out.println("Add date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Add start date DD.MM.YYYY? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
                System.out.println("Enter start date");
                task.setStartDate(stringToDate(serviceLocator.getTerminalService().nextLine()));
            }
            System.out.println("Add end date DD.MM.YYYY? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
                System.out.println("Enter end date");
                task.setEndDate(stringToDate(serviceLocator.getTerminalService().nextLine()));
            }
        }
        serviceLocator.getTaskService().persist(task);
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}