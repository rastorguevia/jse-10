package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter Project ID");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final int number = Integer.parseInt(serviceLocator.getTerminalService().nextLine());
        @Nullable final String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(number, user.getId());
        serviceLocator.getTaskService().removeTaskListByProjectId(projectId);
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}