package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.util.List;

import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class TaskListByEndDateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_list_end";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks by end date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Task list by end date");
        System.out.println("Enter Project ID");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final int number = Integer.parseInt(serviceLocator.getTerminalService().nextLine());
        @Nullable final String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(number, user.getId());
        @NotNull final List<Task> taskListByProjectId = serviceLocator.getTaskService().taskListByProjectId(projectId);
        printTaskListByProjectIdByEndDate(taskListByProjectId);
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}