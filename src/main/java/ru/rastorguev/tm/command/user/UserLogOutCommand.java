package ru.rastorguev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

@NoArgsConstructor
public final class UserLogOutCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "log_out";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User log out.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Log out");
        serviceLocator.getUserService().logOut();
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}