package ru.rastorguev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService extends IService<Task> {

    void removeTaskListByProjectId(final String projectId);

    String getTaskIdByNumber(final int number, final List<Task> filteredListOfTasks);

    List<Task> taskListByUserId(final String userId);

    List<Task> filteredTaskListByUserIdAndInput(final String userId, final String input);

    List<Task> taskListByProjectId(final String projectId);
}
