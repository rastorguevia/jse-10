package ru.rastorguev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAllByUserId(final String userId);

    void removeAllByUserId(final String userId);

    String getProjectIdByNumber(final int number);

    String getProjectIdByNumberForUser(final int number, final String userId);

    List<Project> findProjectsByInputAndUserId(final String input, final String userId);
}
